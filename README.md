# Ads.com Code-Challenge

## Build Setup

```bash
# clone repo
$ git clone https://bitbucket.org/bodisteam/code-challenge-js.git code-challenge

# got to project path
$ cd path/to/code-challenge

# enable .env
$ cp example.env .env

# install dependencies
$ npm install

# serve app at localhost:4000
$ npm run dev

# serve api at localhost:3001
$ npx json-server --watch --port=3001 db.json
```
Navigate to http://localhost:4000

## Code Challenge
- Explore the project sandbox by clicking though every actionable button.
- Navigate to `http://localhost:4000/code-challenge` and complete the todo list.
- Do a thorough project search for inline TODO comments.
- Identify things that can be improved in addition to the todo list.

## Submit Changes.
- Create a new branch and commit your changes under your new branch.
- Once completed, please create a pull request and notify your interviewer.
